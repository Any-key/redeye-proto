package com.redeye.calculator;

import java.util.Random;

public class Utils {
	static String[] names = {"Abraham", "Ahmad", "Antione", "Barrett", "Benton", "Bradly", "Brooks", "Cedrick", "Christian",
			"Claudio", "Clay", "Coleman", "Dallas", "Damian", "Daren", "Dwayne", "Dwight", "Eduardo", "Emerson",
			"Erik", "Floyd", "Graig", "Graham", "Jackie", "Jason", "Jeffery", "Jimmie", "Johnny", "Ken", "Leopoldo",
			"Lon", "Marvin", "Morgan", "Randall", "Refugio", "Ricardo", "Ronny", "Rudolf", "Rudy", "Rupert", "Russel",
			"Sergio", "Sherman", "Ted", "Terrance", "Theron", "Vaughn", "Vernon", "Zachariah",

			"Ana", "Angelena", "Armanda", "Asley", "Audrey", "Bernita", "Betsey", "Brittany", "Catrice", "Charmaine",
			"Claudine", "Dayle", "Dell", "Delma", "Dulcie", "Elenor", "Elenore", "Elly", "Emelina", "Emilee",
			"Eun", "Gala", "Holli", "Hyon", "Irma", "Lauri", "Loreta", "Lue", "Maia", "Marcene", "Mariel", "Maryanne",
			"Marybelle", "Matilde", "Merilyn", "Nichol", "Niesha", "Norine", "Romaine", "Rosalie", "Shaunda",
			"Shemeka", "Sherril", "Syreeta", "Temika", "Tessa", "Toya", "Una", "Vernita", "Wendy"};

	static String[] surnames  = {"Adams", "Allen", "Anderson", "Andrews", "Armstrong", "Arnold", "Austin", "Bailey", "Baker",
			"Banks", "Barker", "Barnes", "Barnett", "Barrett",  "Bates", "Beck", "Becker", "Bell", "Bennett", "Berry",
			"Bishop", "Black", "Bowman", "Boys", "Bradley", "Brewer", "Brooks", "Brown", "Burns", "Burton",
			"Butler", "Byrd", "Campbell", "Carlson", "Carpenter", "Carr", "Carroll", "Carter", "Castro", "Chambers",
			"Chapman", "Chen", "Clark", "Cohen", "Cole", "Coleman", "Collins", "Cook", "Cooper", "Cox", "Craig",
			"Crawford", "Cruz", "Curtis", "Daniels", "Davidson", "Davis", "Day", "Dean", "Delgado", "Diaz", "Dixon",
			"Douglas", "Duncan", "Dunn", "Edwards", "Ellis", "Evans", "Ferguson", "Fernandez", "Fields", "Fisher",
			"Fleming", "Flores", "Ford", "Foster", "Fowler", "Fox", "Franklin", "Freeman", "Fuller", "Garcia",
			"Gardner", "Garrett", "George", "Gibson", "Gilbert", "Gomez", "Ginzales", "Gordon", "Graham", "Grant",
			"Gray", "Green", "Gregory", "Griffin", "Gutierrez", "Guzman", "Hall", "Hamilton", "Hansen", "Harper",
			"Harris", "Harrison", "Hart", "Harvey", "Hawkins", "Hayes", "Henderson", "Hernandez", "Herrera", "Hicks",
			"Hill ", "Hoffman", "Holland", "Holmes", "Holt", "Hopkins", "Howard", "Howell", "Hudson", "Hughes", "Hunt",
			"Hunter", "Jackson", "Jacobs", "James", "Jenkins", "Jensen", "Johnson", "Jones", "Jordan", "Keller",
			"Kelly", "Kennedy", "Kim", "King", "Knight", "Lambert", "Lane", "Larson", "Lawrence", "Lawson", "Lee",
			"Leonard", "Lewis", "Little", "Long", "Lopez", "Lowe", "Lucas", "Lynch", "Maldonado", "Marshall", "Martin",
			"Martinez", "Mason", "Matthews", "May", "McCoy", "McDaniel", "McDonald", "Medina", "Mendez", "Mendoza",
			"Meyer", "Miller", "Mills", "Mitchell", "Montgomery", "Moore", "Morales", "Moreno", "Morgan", "Morris",
			"Morrison", "Munoz", "Murphy", "Murray", "Myers", "Nelson", "Newman", "Nguyen", "Nichols", "Norris",
			"Nunez", "OBrien", "Oliver", "Olson", "Ortega", "Ortiz", "Owens", "Palmer", "Parker", "Parks", "Patel",
			"Patterson", "Paybe", "Pearson", "Pena",  "Perez", "Perkins", "Perry", "Peters", "Peterson", "Phillips",
			"Pierce", "Porter", "Powell", "Powers", "Price", "Ramirez", "Ramos", "Ray", "Reed", "Reyes", "Reynolds",
			"Rhodes", "Rice", "Richards", "Richardson", "Riley", "Rios", "Rivera", "Roberts", "Robertson", "Robinson",
			"Rodriguez", "Rogers", "Romero", "Rose", "Ross", "Russell", "Ryan", "Salazar", "Sanchez", "Sanders",
			"Santiago", "Santos", "Schmidt", "Schultz", "Schwartz", "Scott", "Shaw", "Shelton", "Silva", "Simmons",
			"Simpson", "Smith", "Sims", "Snyder", "Soto", "Spencer", "Stanley", "Stephens", "Stevens", "Stewart",
			"Stone", "Sullivan", "Sutton", "Taylor", "Thomas", "Thompson",	"Torres", "Tran", "Tucker", "Turner",
			"Vega", "Wade", "Wagner", "Walker", "Wallace", "Walsh", "Walters", "Ward", "Warren", "Washington",
			"Watkins", "Watson", "Watts", "Weaver", "Webb", "Weber", "Welch", "Wells", "West", "Wheeler", "White",
			"Williams", "Willis", "Wilson", "Wong", "Woods", "Wright", "Young"};

	static String[] projectNames = {"Warty Warthog", "Hoary Hedgehog", "Breezy Badger", "Dapper Drake", "Edgy Eft",
			"Feisty Fawn", "Gutsy Gibbon", "Hardy Heron", "Intrepid Ibex", "Jaunty Jackalope", "Karmic Koala",
			"Lucid Lynx", "Maverick Meerkat", "Natty Narwhal", "Oneiric Ocelot", "Precise Pangolin", "Quantal Quetzal",
			"Raring Ringtail", "Saucy Salamander", "Trusty Tahr", "Utopic Unicorn", "Vivid Vervet", "Wily Werewolf",
			"Xenial Xerus", "Yakkety Yak"};

	static public String generateName() {
		String name = names[new Random().nextInt(names.length)];
		String surname = surnames[new Random().nextInt(surnames.length)];
		return name + " " + surname;
	}

	static public String generateProjectName() {
		return projectNames[new Random().nextInt(projectNames.length)];
	}

}